import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Moeda, Conversao, ConversaoResponse } from '../models';
import { MoedaService, ConversorService } from '../services';


@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

  public moedas: Moeda[];
  public conversao: Conversao;
  public possuiErro: boolean;
  public conversaoResponse: ConversaoResponse;
  public selectDisabled: boolean;

  @ViewChild('conversaoForm', { static: true }) conversaoForm: NgForm;


  constructor(
    private moedaService: MoedaService,
    public conversaoService: ConversorService
  ) { }

  ngOnInit() {
    this.moedas = this.moedaService.listarTodas();
    this.init();
  }

  /**
   * Efetua a chamada para a conversao dos valores.
   *
   * @return void
   */
  init(): void {
    this.conversao = new Conversao('EUR', 'BRL', null);
    this.possuiErro = false;
    this.selectDisabled = true;
  }
/**
 * Efetua a chamada a conversao dos valores;
 *
 * @return void
 */
converter(): void {
  if (this.conversaoForm.form.valid) {
    this.conversaoService
      .converter(this.conversao)
      .subscribe(
        response => this.conversaoResponse = response,
        error => this.possuiErro = true
      );
  }
}
}
