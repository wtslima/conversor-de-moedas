export class ConversaoResponse {
    constructor(
        public base: string,
        public date: string,
        public rates: any
    ) {}
}

// tslint:disable-next-line: eofline
// {"base":"USD", "date":"2019-08-20", "rates":{4.01237}}